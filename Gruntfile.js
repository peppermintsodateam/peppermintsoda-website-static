module.exports = function(grunt) {
	grunt.initConfig({

		sass:{
			dev:{
				options:{
					style:"expanded"

				},

      			src:"src/scss/bundle.scss",
      			dest:"src/css/base.css"
			}
		},

		cssmin: {
			prod:{
				files: {
					"build/css/base.css" : "src/css/base.css",
					"build/css/screen.css" : "src/css/screen.css"
				}
			}
		},


		uglify: {

			prod:{
				options:{
					mangle:false//aby zachowac nazwy zmiennych
				},

				files:{
					"build/js/scripts.js": "src/js/scripts.js",
					"build/js/staff.js": "src/js/staff.js",
					"build/js/plugins.js": "src/js/plugins.js"
				}
			}	
		},


		concat: {

			options: {
				separator: ';',
			},

			prod: {
				src: ['src/js/slick.min.js', 'src/js/jquery.counterup.js', 'src/js/jquery.magnific-popup.min.js'],
				dest: 'src/js/plugins.js'
			}
		},


		concat_css: {
		    options: {
		      // Task-specific options go here. 
		    },

		    prod: {
		      src: ["src/css/animate.min.css", "src/css/slick-theme.css", "src/css/slick.css", "src/css/magnific-popup.css"],
		      dest: "src/css/screen.css"
		    },
  		},


  		autoprefixer: {

			dev: {
				options: {
					browsers:["last 25 version"]
				},

				src:"src/css/*.css"
			}

		},


		watch:{
			options:{
				livereload:true
			},

			dev:{
				files:["src/**/*"],
				tasks:["dev"] 
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');


	grunt.registerTask("dev",["sass","autoprefixer"]);
	grunt.registerTask("prod",["uglify","cssmin","concat","concat_css"]);//server produkcyjny
	grunt.registerTask("default","dev");//po wpisaniu grunt, wykonaja si tylko wersja developerska
	grunt.registerTask("build",["dev","prod"]);
}