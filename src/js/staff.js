(function($){

    


function teamHover(a) {
    $("#" + a + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + a + "-h.gif")
}
function teamLeave(a) {
    $("#" + a + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + a + ".gif")
}
function teamInfo(a) {
    var b = $("#" + a);
    if (!b.hasClass("clicked")) {
        b.addClass("clicked")
    } else {
        b.removeClass("clicked")
    }
}
function mdInfo(b) {
    var a = $("#" + b + " .content");
    if (!a.hasClass("clicked")) {
        a.addClass("clicked").fadeIn(300)
    } else {
        a.removeClass("clicked").fadeOut(300)
    }
}
function teamAnimate() {
    var staff = ["suzy", "bryan", "jane", "rachael-w", "jp", "lucy", "tamsin", "lexi", "steph", "emmaw", "sophie", "ben", "jo", "lucyh", "cat", "john", "julie", "andy", "megan"];

    var c = staff[Math.floor(Math.random() * staff.length)];
    var a = staff[Math.floor(Math.random() * staff.length)];
    var b = staff[Math.floor(Math.random() * staff.length)];
    var d = staff[Math.floor(Math.random() * staff.length)];
    var e = staff[Math.floor(Math.random() * staff.length)];
    var f = staff[Math.floor(Math.random() * staff.length)];
   

    $("#" + a + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + a + "-h.gif");
    setTimeout(function() {
        $("#" + a + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + a + ".gif")
    }, 5000);
    $("#" + b + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + b + "-h.gif");
    setTimeout(function() {
        $("#" + b + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + b + ".gif")
    }, 5000);
    $("#" + c + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + c + "-h.gif");
    setTimeout(function() {
        $("#" + c + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + c + ".gif")
    }, 5000);
    $("#" + d + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + d + "-h.gif");
    setTimeout(function() {
        $("#" + d + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + d + ".gif")
    }, 5000);
    $("#" + e + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + e + "-h.gif");
    setTimeout(function() {
        $("#" + e + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + e + ".gif")
    }, 5000);

    $("#" + f + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + f + "-h.gif");
    setTimeout(function() {
        $("#" + f + " img").attr("src", "http://www.peppermintsoda.co.uk/wordpress/wp-content/themes/peppermint_theme/assets/img/staff/" + f + ".gif")
    }, 5000);

}


function loadTeam(){
    var staff     = ["suzy", "bryan", "jane", "rachael-w", "jp", "lucy", "tamsin", "steph", "emmaw", "lexi", "sophie", "ben", "jo", "lucyh", "cat", "john", "andy", "julie", "megan"];
    var container = $('#staff-rotator');

    $.each(staff, function(index, item){

        container.append('<a href="/team" id="'+item+'" class="staff-member"><img src="/images/staff/'+item+'.gif" alt="'+item+'" /></a>');

    });
}

// Define variables
        var team;

        // When document is ready
       $(function(){


            // When team memebers are hovered, change image src
            $(".team").hover(function(e){
                e.preventDefault();
                var team = $(this).attr('id');
                var span = $(this).find('span');
                teamHover(team);
                span.show();
            }, function(e) {
                e.preventDefault();
                var team = $(this).attr('id');
                var span = $(this).find('span');
                teamLeave(team);
                span.hide();
            });

            // When team memebers are clicked, show intro
            $(".team").click(function(e){
                e.preventDefault();
                var team = $(this).attr('id');
                var span = $(this).find('span');
                teamInfo(team);
                span.hide();
            });

            // When MDs are hovered, change image src
            $(".md").hover(function(e){
                e.preventDefault();
                var md   = $(this).attr('id');
                var span = $(this).find('span');
                teamHover(md);
                span.show();
            }, function(e) {
                e.preventDefault();
                var md   = $(this).attr('id');
                var span = $(this).find('span');
                teamLeave(md);
                span.hide();
            });

            // When MDs are clicked, show bio
            $(".md").click(function(e){
                e.preventDefault();
                var md   = $(this).attr('id');
                var span = $(this).find('span');
                mdInfo(md);
                span.hide();
            });

            // Randomly change team images every 4 seconds
            setInterval(function() {
                teamAnimate();
            }, 6000);

        });

})(jQuery);





